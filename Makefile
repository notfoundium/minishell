# Shell name
NAME = minishell

# Cosmetics
SH_PREFIX = [MiniShell]
SH_ECHO = echo $(SH_PREFIX)

# CC configs
CC =		clang
CFLAGS =	-Wall -Wextra -Werror

# Project structure
LFT_DIR =	./libft/
SRC_DIR =	./src/
OBJ_DIR =	./obj/
INC_DIR =	./include/

# MiniShell modules
MSH_MOD =	builtins\
			core

# Virtual path config
vpath %.c $(addprefix $(SRC_DIR), $(MSH_MOD))
vpath %.o $(OBJ_DIR)

# Source config
CORE =		main\
			isvalid_token\
			cmd\
			env\
			utils\
			prompt\
			pipe\
			exec\
			child\
			signal\
			path\
			pid\
			token\
			read_symbol\
			token_reader\
			token_addstr\
			token_addenv\
			push\
			parser\
			strenv\
			get_next_token\
			separate_cmd\
			parse_cmd\
			cmdtok\
			add_op

# Builtins source config
BUILTINS =	b_pwd\
			b_exit\
			b_env\
			b_echo\
			b_cd\
			b_export\
			b_unset

# Includes
INC = -I./include/ -I$(LFT_DIR)/include/

# Library
LIB = -L$(LFT_DIR) -lft

# Objects
SRC =	$(CORE) $(BUILTINS)
OBJ =	$(addsuffix .o, $(SRC))

# Make rules
all: $(NAME)

%.o: %.c
	@$(CC) $(CFLAGS) $(INC) -c $< -O0 -g -fsanitize=address

lib:
	@$(MAKE) -C $(LFT_DIR) all

$(NAME): lib $(OBJ)
	@$(CC) $(CFLAGS) $(OBJ) $(LIB) -o $(NAME) -O0 -g -fsanitize=address

clean:
	@$(MAKE) -C $(LFT_DIR) clean
	@rm -rf *.o

fclean:
	@$(MAKE) -C $(LFT_DIR) fclean
	@rm -rf *.o
	@rm -rf $(NAME)

re: fclean all
