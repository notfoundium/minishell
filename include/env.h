/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:26:21 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:26:26 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENV_H
# define ENV_H

# include "ft_stdlib.h"
# include "ft_vector.h"

void	env_init(char **envp);
size_t	get_env_size(char **env);
size_t	get_key_len(const char *pair);
char	*get_env(char *key);
int		env_add(char *pair);
char	*find_path(char *cmd);

#endif
