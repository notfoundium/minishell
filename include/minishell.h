/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:46:55 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:46:56 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <stdlib.h>
# include "env.h"
# include "cmd.h"
# include "token.h"

typedef	unsigned int	t_flag;

int						g_status;

void					run_minishell(t_token *token_list);
int						exec(char **av, int fd_in, int fd_out, int *fdlist);
int						execute_all(t_cmd *cmd);
void					show_prompt(void);
char					*get_wd(void);
void					signal_dispatcher();
char					**get_path_arr(void);
int						*new_pipe(int pipe_count);
t_cmd					*read_cmd(char c);
t_vector				*cmdparse(t_vector *tokens);

#endif
