/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:30:41 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:30:43 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXEC_H
# define EXEC_H

# include "token.h"
# include "cmd.h"

void	exec_line(t_token *token);
void	exec_cmd(t_cmd *cmd);
int		run_child(char **av, int fd_in, int fd_out, int *fdlist);
int		run_builtin(char **av, int fd_in, int fd_out);
int		is_builtin(char *cmd);

#endif
