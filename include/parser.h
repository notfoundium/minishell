/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:43:23 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:43:28 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include "minishell.h"
# include "ft_basic_string.h"

typedef enum	e_mode
{
	M_DEF = 1,
	M_SOFT = 1 << 1,
	M_HARD = 1 << 2
}				t_mode;

typedef enum	e_readflag
{
	RD_OK = 0,
	RD_EXIT = 1,
	RD_SYNTAX = 2
}				t_readflag;

int		new_parser(t_string **str, t_vector **argv);
int		delete_parser(t_string **str, t_vector **argv, int flag);
int		parse_cmd(t_token *token, int *index);
t_token	*pushstr(t_string *str, t_token *token, int *is_append);
t_token	*pushenv(t_string *str, t_token *token, int *is_append);
t_token	*pushspace(t_string *str, t_vector *argv, t_token *token,
					int *is_append);
t_token	*pushpipe(t_string *str, t_token *token,
				t_vector **argv, int *is_append);
t_token	*pushredir(t_token *token);
int		separate_cmd(t_string *str, t_vector *argv,
					int is_append, int *index);
char	*get_next_token(t_token **token);
char	*strenv(char *key);
void	token_addstr(t_token *token_list, int *c);
void	token_addenv(t_token *token_list, int *c);
t_token	*cmdtok();
char	**get_path_arr(void);

#endif
