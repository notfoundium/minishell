/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:23:42 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:23:44 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTINS_H
# define BUILTINS_H

int		b_echo(char **av);
int		b_cd(char **av);
int		b_pwd(void);
int		b_export(char **av, int fd_out);
int		b_unset(char **av);
int		b_env(void);
int		b_exit(char **av);

#endif
