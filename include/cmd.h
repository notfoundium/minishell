/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:27:27 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:27:29 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CMD_H
# define CMD_H

typedef struct		s_cmd
{
	char			**argv;
	int				fd_in;
	int				fd_out;
	struct s_cmd	*next;
}					t_cmd;

t_cmd				*g_cmd;
int					g_fd[2];

t_cmd				*new_cmd(void);
void				cmd_push(t_cmd *cmd, char **argv);
int					cmd_count(t_cmd *cmd);
void				delete_cmd(t_cmd *commands);
void				cmd_print(t_cmd *cmd);

#endif
