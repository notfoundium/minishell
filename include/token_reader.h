/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_reader.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:43:08 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:43:09 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOKEN_READER_H
# define TOKEN_READER_H

# include "ft_basic_string.h"
# include "token.h"
# include "parser.h"

int		read_env(int *c, t_string *str, t_token *token_list);
int		read_soft(int *c, t_string *str, t_mode *mode);
int		read_hard(int *c, t_string *str, t_mode *mode);
int		read_escape_def(int *c, t_string *str);
int		read_escape_soft(int *c, t_string *str);
int		read_symbol(int *c, t_string *str);

#endif
