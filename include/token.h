/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:44:04 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:44:05 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOKEN_H
# define TOKEN_H

typedef enum		e_toktype
{
	T_SPACE = 0,
	T_STR = 1,
	T_ENV = 2,
	T_PIPE = 3,
	T_SEMICOL = 4,
	T_REDIR_IN = 5,
	T_REDIR_OUT = 6,
	T_REDIR_APPEND = 7
}					t_toktype;

typedef struct		s_token
{
	char			*str;
	t_toktype		type;
	struct s_token	*next;
}					t_token;

t_token				*new_token(void);
void				token_push(t_token *tok, char *str,
								int type);
void				token_print(t_token *token);
void				delete_token(t_token *token);
void				token_pop(t_token *token);
int					token_isvalid(t_token *token);

#endif
