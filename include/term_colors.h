/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_colors.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:27:51 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:28:20 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TERM_COLORS_H
# define TERM_COLORS_H

# define COL_DEFAULT "\x01b[0m"
# define COL_ORANGE "\x01b[91m"
# define COL_BLUE "\x01b[96m"

#endif
