/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pid.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/22 17:01:22 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/22 17:01:23 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PID_H
# define PID_H

# include <unistd.h>
# include "ft_stdlib.h"

typedef struct	s_pidlist
{
	int		*list;
	size_t	count;
}				t_pidlist;

t_pidlist		*volatile g_pidlist;

t_pidlist		*new_pidlist(void);
void			delete_pidlist(void);
void			pidlist_resize(size_t new_size);
void			pidlist_push(int pid);

#endif
