/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:56:04 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 03:56:05 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "ft_basic_string.h"
#include "errno.h"
#include "ft_stdlib.h"
#include "ft_stdio.h"
#include "ft_ctype.h"
#include "ft_etc.h"

void		add_op(t_token *token, int *c)
{
	char	op[3];

	op[0] = (char)*c;
	op[1] = 0;
	op[2] = 0;
	if (*c == '>')
	{
		*c = ft_getchar();
		if (*c == '>')
		{
			op[1] = '>';
			*c = ft_getchar();
		}
	}
	else
		*c = ft_getchar();
	token_push(token, op, 0);
}
