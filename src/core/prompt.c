/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 21:18:45 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/06 21:18:47 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "term_colors.h"
#include "ft_stdio.h"
#include "ft_string.h"

void	show_prompt(void)
{
	ft_write(1, COL_ORANGE, ft_strlen(COL_ORANGE));
	ft_write(1, "[", 1);
	ft_write(1, COL_BLUE, ft_strlen(COL_BLUE));
	ft_write(1, "tiSH", 4);
	ft_write(1, COL_ORANGE, ft_strlen(COL_ORANGE));
	ft_write(1, "] $> ", 5);
	ft_write(1, COL_DEFAULT, ft_strlen(COL_DEFAULT));
}
