/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_addenv.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 16:14:12 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 16:14:13 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "ft_basic_string.h"
#include "errno.h"
#include "ft_stdlib.h"
#include "ft_stdio.h"
#include "ft_ctype.h"
#include "ft_etc.h"

void	token_addenv(t_token *token_list, int *c)
{
	t_string	*str;

	if (*c == '?')
	{
		token_push(token_list, "?", T_ENV);
		*c = ft_getchar();
		return ;
	}
	str = new_string(0);
	while (ft_isalnum(*c) || *c == '_')
	{
		string_pushback(str, *c);
		*c = ft_getchar();
	}
	token_push(token_list, str->ptr, T_ENV);
	delete_string(str);
}
