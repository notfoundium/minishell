/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_reader.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 16:14:31 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 16:14:32 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "ft_basic_string.h"
#include "errno.h"
#include "ft_stdlib.h"
#include "ft_stdio.h"
#include "ft_ctype.h"
#include "ft_etc.h"

int		read_env(int *c, t_string *str, t_token *token_list)
{
	*c = ft_getchar();
	if (!ft_isalpha(*c) && *c != '_' && *c != '?')
	{
		string_pushback(str, '$');
		return (1);
	}
	if (str->ptr[0])
		token_push(token_list, str->ptr, T_STR);
	string_clear(str);
	token_addenv(token_list, c);
	return (0);
}

int		read_soft(int *c, t_string *str, t_mode *mode)
{
	if (*mode == M_DEF)
		*mode = M_SOFT;
	else if (*mode == M_SOFT)
		*mode = M_DEF;
	else if (*mode == M_HARD)
		string_pushback(str, *c);
	*c = ft_getchar();
	return (1);
}

int		read_hard(int *c, t_string *str, t_mode *mode)
{
	if (*mode == M_DEF)
		*mode = M_HARD;
	else if (*mode == M_HARD)
		*mode = M_DEF;
	else if (*mode == M_SOFT)
		string_pushback(str, *c);
	*c = ft_getchar();
	return (1);
}

int		read_escape_def(int *c, t_string *str)
{
	*c = ft_getchar();
	string_pushback(str, *c);
	*c = ft_getchar();
	return (1);
}

int		read_escape_soft(int *c, t_string *str)
{
	*c = ft_getchar();
	if (*c != '\"' && *c != '$')
		string_pushback(str, '\\');
	string_pushback(str, *c);
	*c = ft_getchar();
	return (1);
}
