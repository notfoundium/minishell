/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isvalid_token.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:30:12 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:09:02 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "ft_stdio.h"
#include "ft_string.h"
#include "exec.h"
#include "parser.h"

static int	is_redir(t_toktype type)
{
	return (type == T_REDIR_APPEND ||
			type == T_REDIR_IN ||
			type == T_REDIR_OUT);
}

int			token_isvalid(t_token *token)
{
	t_toktype	prev;

	if (token->type == T_PIPE || token->type == T_SEMICOL)
		return (0);
	prev = token->type;
	token = token->next;
	while (token)
	{
		if (token->type == T_PIPE && !token->next)
			return (0);
		if (token->type == T_SPACE)
		{
			token = token->next;
			continue;
		}
		if ((token->type == T_PIPE || token->type == T_SEMICOL)
		&& (prev == T_PIPE || prev == T_SEMICOL))
			return (0);
		if (is_redir(prev) && (token->type != T_ENV && token->type != T_STR))
			return (0);
		prev = token->type;
		token = token->next;
	}
	return (!is_redir(prev));
}
