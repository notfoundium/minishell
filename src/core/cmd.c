/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:19:25 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 03:19:26 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd.h"
#include "utils.h"
#include "ft_stdlib.h"
#include "ft_stdio.h"
#include "ft_string.h"
#include <stdio.h>

t_cmd	*new_cmd(void)
{
	t_cmd	*cmd;

	cmd = (t_cmd*)ft_malloc(sizeof(t_cmd));
	cmd->argv = NULL;
	cmd->next = NULL;
	cmd->fd_in = 0;
	cmd->fd_out = 1;
	return (cmd);
}

void	delete_cmd(t_cmd *cmd)
{
	t_cmd	*tmp;

	while (cmd)
	{
		delete_arr(cmd->argv);
		tmp = cmd;
		cmd = cmd->next;
		free(tmp);
	}
}

void	cmd_push(t_cmd *cmd, char **argv)
{
	if (!cmd->argv)
	{
		cmd->argv = argv;
		cmd->fd_in = g_fd[0];
		cmd->fd_out = g_fd[1];
		g_fd[0] = 0;
		g_fd[1] = 1;
		return ;
	}
	while (cmd->next)
		cmd = cmd->next;
	cmd->next = new_cmd();
	cmd->next->argv = argv;
	cmd->next->fd_in = g_fd[0];
	cmd->next->fd_out = g_fd[1];
	g_fd[0] = 0;
	g_fd[1] = 1;
}

int		cmd_count(t_cmd *cmd)
{
	int	count;

	count = 0;
	while (cmd)
	{
		cmd = cmd->next;
		count++;
	}
	return (count);
}

void	cmd_print(t_cmd *cmd)
{
	int	i;

	while (cmd)
	{
		i = 0;
		ft_write(1, "{ ", 2);
		while (cmd->argv[i])
		{
			ft_write(1, "[", 1);
			ft_write(1, cmd->argv[i], ft_strlen(cmd->argv[i]));
			ft_write(1, "]", 1);
			ft_write(1, " ", 1);
			i++;
		}
		ft_write(1, "}\n", 2);
		cmd = cmd->next;
	}
}
