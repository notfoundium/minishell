/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_addstr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 15:58:39 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 15:58:40 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "ft_basic_string.h"
#include "errno.h"
#include "ft_stdlib.h"
#include "ft_stdio.h"
#include "ft_ctype.h"
#include "ft_etc.h"
#include "token_reader.h"

static int	is_tokdelim(int c)
{
	return (c == ' ' || c == '\n' || c == ';' ||
			c == '|' || c == '<' || c == '>');
}

void		token_addstr(t_token *token_list, int *c)
{
	t_string	*str;
	t_mode		mode;
	t_flag		append;

	str = new_string(0);
	mode = M_DEF;
	append = 0;
	while (mode != M_DEF || !is_tokdelim(*c))
	{
		if (mode != M_HARD && *c == '$')
			append = read_env(c, str, token_list);
		else if (*c == '\'')
			append = read_hard(c, str, &mode);
		else if (*c == '\"')
			append = read_soft(c, str, &mode);
		else if (*c == '\\' && mode == M_DEF)
			append = read_escape_def(c, str);
		else if (*c == '\\' && mode == M_SOFT)
			append = read_escape_soft(c, str);
		else
			append = read_symbol(c, str);
	}
	if (append)
		token_push(token_list, str->ptr, T_STR);
	delete_string(str);
}
