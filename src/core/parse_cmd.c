/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cmd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 20:23:43 by mwillena          #+#    #+#             */
/*   Updated: 2021/01/10 20:23:49 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "sys/types.h"
#include "ft_stdio.h"
#include "unistd.h"
#include "builtins.h"
#include "pid.h"
#include "utils.h"
#include "minishell.h"
#include "sys/wait.h"
#include "ft_basic_string.h"
#include "ft_etc.h"
#include "parser.h"

static int	is_tok_redir(t_toktype type)
{
	return (type == T_REDIR_IN ||
			type == T_REDIR_OUT ||
			type == T_REDIR_APPEND);
}

int			is_str_or_redir(t_toktype type)
{
	return (type == T_STR ||
			type == T_ENV ||
			type == T_SPACE ||
			is_tok_redir(type));
}

t_token		*parse_base(t_token *token, t_string *str,
						t_vector *argv, int *is_append)
{
	if (token->type == T_STR)
		return (pushstr(str, token, is_append));
	else if (token->type == T_ENV)
		return (pushenv(str, token, is_append));
	else if (token->type == T_SPACE)
		return (pushspace(str, argv, token, is_append));
	else if (is_tok_redir(token->type))
		token = pushredir(token);
	return (token);
}

t_token		*skip_tokens(t_token *token, int *index)
{
	int	i;

	i = 0;
	while (i < *index && token)
	{
		if (token->type == T_SEMICOL)
			i++;
		token = token->next;
	}
	return (token);
}

int			parse_cmd(t_token *token, int *index)
{
	t_vector	*argv;
	t_string	*str;
	int			is_append;

	g_fd[0] = 0;
	g_fd[1] = 1;
	if (!(token = skip_tokens(token, index)))
		return (1);
	is_append = new_parser(&str, &argv);
	while (token)
		if (token->type == T_SEMICOL)
			return (separate_cmd(str, argv, is_append, index));
		else if (is_str_or_redir(token->type))
			token = parse_base(token, str, argv, &is_append);
		else if (token->type == T_PIPE)
			token = pushpipe(str, token, &argv, &is_append);
		else
			token = token->next;
	if (is_append)
		vector_push_str(argv, str->ptr);
	cmd_push(g_cmd, (char**)vector_cp_ptr(argv));
	if (!argv->ptr[0])
		return (delete_parser(&str, &argv, 1));
	delete_parser(&str, &argv, 0);
	return (*index = *index + 1);
}
