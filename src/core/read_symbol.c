/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_symbol.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 20:24:34 by mwillena          #+#    #+#             */
/*   Updated: 2021/01/10 20:24:37 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_basic_string.h"
#include "ft_stdio.h"

int		read_symbol(int *c, t_string *str)
{
	string_pushback(str, *c);
	*c = ft_getchar();
	return (1);
}
