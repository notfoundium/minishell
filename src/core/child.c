/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   child.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:36:26 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/17 18:50:45 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "sys/types.h"
#include "ft_stdio.h"
#include "builtins.h"
#include "pid.h"
#include "utils.h"
#include "minishell.h"
#include "sys/wait.h"
#include "ft_basic_string.h"
#include "ft_etc.h"
#include "parser.h"

int	run_cmd_builtin(char *cmd, char **argv)
{
	if (!ft_strcmp(cmd, "pwd"))
		return (b_pwd());
	else if (!ft_strcmp(cmd, "env"))
		return (b_env());
	else
		return (b_echo(argv));
}

int	run_cmd(char **argv)
{
	int		ret;
	char	*cmd;
	char	*tmp;

	cmd = argv[0];
	if (!argv[0][0])
		return (0);
	if (!ft_strcmp(cmd, "pwd") ||
		!ft_strcmp(cmd, "env") || !ft_strcmp(cmd, "echo"))
		ret = run_cmd_builtin(cmd, argv);
	else
	{
		if (!(tmp = find_path(cmd)))
		{
			ft_write(2, cmd, ft_strlen(cmd));
			ft_write(2, ": command not found\n", 20);
			return (127);
		}
		if ((ret = execve(tmp, argv, (char**)g_env->ptr)) < 0)
		{
			free(tmp);
			ft_perror(cmd);
		}
	}
	return (ret);
}

int	is_builtin(char *cmd)
{
	if (!ft_strcmp(cmd, "cd") ||
		!ft_strcmp(cmd, "unset") ||
		!ft_strcmp(cmd, "export") ||
		!ft_strcmp(cmd, "exit"))
		return (1);
	return (0);
}

int	run_builtin(char **av, int fd_in, int fd_out)
{
	if (!ft_strcmp(av[0], "cd"))
		g_status = b_cd(av);
	else if (!ft_strcmp(av[0], "unset"))
		g_status = b_unset(av);
	else if (!ft_strcmp(av[0], "export"))
		g_status = b_export(av, fd_out);
	else if (!ft_strcmp(av[0], "exit"))
		g_status = b_exit(av);
	if (fd_in != 0)
		close(fd_in);
	if (fd_out != 1)
		close(fd_out);
	return (0);
}

int	run_child(char **av, int fd_in, int fd_out, int *fdlist)
{
	int	status;
	int	i;

	if (fd_in != 0)
	{
		if (dup2(fd_in, 0) < 0)
			ft_perror("dup2");
	}
	if (fd_out != 1)
	{
		if (dup2(fd_out, 1) < 0)
			ft_perror("dup2");
	}
	i = 1;
	while (fdlist && fdlist[i] != 1)
		close(fdlist[i++]);
	status = run_cmd(av);
	delete_arr(av);
	exit(status);
	return (0);
}
