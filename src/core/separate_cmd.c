/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   separate_cmd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:29:12 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:16:01 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "sys/types.h"
#include "ft_stdio.h"
#include "unistd.h"
#include "builtins.h"
#include "pid.h"
#include "utils.h"
#include "minishell.h"
#include "sys/wait.h"
#include "ft_basic_string.h"
#include "ft_etc.h"
#include "parser.h"

int		separate_cmd(t_string *str, t_vector *argv,
					int is_append, int *index)
{
	if (is_append)
		vector_push_str(argv, str->ptr);
	cmd_push(g_cmd, (char**)vector_cp_ptr(argv));
	delete_string(str);
	delete_vector(argv);
	*index = *index + 1;
	return (0);
}
