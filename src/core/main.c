/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:28:36 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:09:39 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include "minishell.h"
#include "ft_string.h"
#include "ft_stdio.h"
#include "parser.h"
#include "token.h"
#include "exec.h"

void	minishell_init(char **envp)
{
	char	*wd;
	char	*tmp;

	env_init(envp);
	g_status = 0;
	wd = get_wd();
	env_add(tmp = ft_strmerge("PWD=", wd));
	free(wd);
	free(tmp);
	signal_dispatcher();
}

void	run_minishell(t_token *token)
{
	while (1)
	{
		show_prompt();
		token = cmdtok();
		if (!token)
		{
			if (errno == 0)
			{
				ft_write(1, "\n", 1);
				break ;
			}
			continue;
		}
		else if (!token->str)
		{
			delete_token(token);
			continue;
		}
		if (!token_isvalid(token))
			ft_puts("Syntax error!");
		else
			exec_line(token);
		delete_token(token);
	}
}

int		main(int argc, char **argv, char **envp)
{
	t_token	*token_list;

	(void)argc;
	(void)argv;
	token_list = NULL;
	minishell_init(envp);
	run_minishell(token_list);
	delete_vector(g_env);
	return (0);
}
