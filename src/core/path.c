/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 20:24:12 by mwillena          #+#    #+#             */
/*   Updated: 2021/01/10 20:24:16 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include "env.h"
#include "utils.h"
#include "minishell.h"
#include "ft_basic_string.h"

static void	push_path(t_vector *vec, t_string *str)
{
	string_pushback(str, '/');
	vector_push(vec, string_c_str(str));
	string_clear(str);
}

char		**get_path_arr(void)
{
	char		*path;
	size_t		i;
	t_string	*str;
	t_vector	*vec;
	char		**arr;

	if (!(path = get_env("PATH")))
		return (0);
	str = new_string(0);
	vec = new_vector();
	i = 0;
	while (path[i])
	{
		if (path[i] != ':')
			string_pushback(str, path[i]);
		else
			push_path(vec, str);
		i++;
	}
	push_path(vec, str);
	delete_string(str);
	arr = (char**)vector_cp_ptr(vec);
	delete_vector(vec);
	return (arr);
}

char		*find_path(char *cmd)
{
	struct stat	sb;
	char		**arr;
	char		*tmp;
	size_t		i;

	if (!ft_strncmp(cmd, "./", 2) || !ft_strncmp(cmd, "/", 1))
		return (ft_strdup(cmd));
	if (!(arr = get_path_arr()))
		return (NULL);
	i = 0;
	while (arr[i])
	{
		tmp = ft_strmerge(arr[i], cmd);
		if (!stat(tmp, &sb))
			return (tmp);
		free(tmp);
		i++;
	}
	delete_arr(arr);
	return (NULL);
}
