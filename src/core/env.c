/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:20:44 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 03:20:45 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdlib.h"
#include "ft_stdio.h"
#include "ft_string.h"
#include "ft_etc.h"
#include "env.h"

void		env_init(char **envp)
{
	size_t	i;

	g_env = new_vector();
	i = 0;
	while (envp[i])
		vector_push_str(g_env, envp[i++]);
}

size_t		get_key_len(const char *pair)
{
	size_t	len;

	len = 0;
	while (pair[len] != '=' && pair[len] != 0)
		len++;
	return (len);
}

static char	*ret_value(char *value)
{
	if (!ft_strcmp(value, ""))
		return (NULL);
	return (value);
}

char		*get_env(char *key)
{
	size_t	i;
	size_t	keylen;
	size_t	current_keylen;

	i = 0;
	keylen = ft_strlen(key);
	while (g_env->ptr[i])
	{
		current_keylen = get_key_len(g_env->ptr[i]);
		if (keylen == current_keylen)
		{
			if (*((char*)g_env->ptr[i] + keylen) == '=')
				if (!ft_strncmp(key, g_env->ptr[i], keylen))
					return (ret_value(g_env->ptr[i] + keylen + 1));
		}
		i++;
	}
	return (NULL);
}
