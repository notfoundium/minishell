/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmdtok.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 14:49:52 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 14:49:54 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "ft_basic_string.h"
#include "errno.h"
#include "ft_stdlib.h"
#include "ft_stdio.h"
#include "ft_ctype.h"
#include "ft_etc.h"

void		token_addspace(t_token *token_list, int *c)
{
	while (*c == ' ')
		*c = ft_getchar();
	token_push(token_list, " ", T_SPACE);
}

void		token_addredir_out(t_token *token_list, int *c)
{
	*c = ft_getchar();
	if (*c == '>')
	{
		*c = ft_getchar();
		token_push(token_list, ">>", T_REDIR_APPEND);
		return ;
	}
	token_push(token_list, ">", T_REDIR_OUT);
}

void		token_addop(t_token *token_list, int *c)
{
	if (*c == ';')
		token_push(token_list, ";", T_SEMICOL);
	else if (*c == '|')
		token_push(token_list, "|", T_PIPE);
	else if (*c == '<')
		token_push(token_list, "<", T_REDIR_IN);
	*c = ft_getchar();
}

t_toktype	token_get_last_type(t_token *token)
{
	t_toktype	ret;

	while (token)
	{
		ret = token->type;
		token = token->next;
	}
	return (ret);
}

t_token		*cmdtok(void)
{
	t_token		*token_list;
	int			c;

	errno = 0;
	if ((c = ft_getchar()) < 0)
		return (NULL);
	token_list = new_token();
	while (c == ' ')
		c = ft_getchar();
	while (c > 0 && c != '\n')
	{
		if (c == ' ')
			token_addspace(token_list, &c);
		if (c == ';' || c == '|' || c == '<')
			token_addop(token_list, &c);
		else if (c == '>')
			token_addredir_out(token_list, &c);
		else
			token_addstr(token_list, &c);
	}
	if (token_get_last_type(token_list) == T_SPACE)
		token_pop(token_list);
	return (token_list);
}
