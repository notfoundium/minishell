/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pid.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:21:31 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 03:21:32 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pid.h"
#include "ft_string.h"

t_pidlist	*new_pidlist(void)
{
	t_pidlist	*list;

	list = (t_pidlist*)ft_malloc(sizeof(t_pidlist));
	list->count = 1;
	list->list = (int*)ft_calloc(list->count, sizeof(int));
	return (list);
}

void		delete_pidlist(void)
{
	free(g_pidlist->list);
	free(g_pidlist);
}

void		pidlist_resize(size_t new_count)
{
	int	*new_list;

	new_list = (int*)ft_calloc(new_count, sizeof(int));
	ft_memmove(new_list, g_pidlist->list, g_pidlist->count * sizeof(int));
	free(g_pidlist->list);
	g_pidlist->list = new_list;
	g_pidlist->count = new_count;
}

void		pidlist_push(int pid)
{
	g_pidlist->list[g_pidlist->count - 1] = pid;
	pidlist_resize(g_pidlist->count + 1);
}
