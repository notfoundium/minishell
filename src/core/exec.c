/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:55:57 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:06:21 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "sys/types.h"
#include "ft_stdio.h"
#include "unistd.h"
#include "builtins.h"
#include "pid.h"
#include "utils.h"
#include "minishell.h"
#include "sys/wait.h"
#include "ft_basic_string.h"
#include "ft_etc.h"
#include "parser.h"

int		exec(char **av, int fd_in, int fd_out, int *fdlist)
{
	pid_t	pid;

	if (is_builtin(av[0]))
		return (run_builtin(av, fd_in, fd_out));
	pid = fork();
	if (pid < 0)
		return (1);
	if (pid == 0)
		return (run_child(av, fd_in, fd_out, fdlist));
	else
	{
		if (fd_in != 0)
			close(fd_in);
		if (fd_out != 1)
			close(fd_out);
		pidlist_push(pid);
		return (0);
	}
}

void	fd_apply(t_cmd *cmd, int *fdlist)
{
	int	i;

	i = 0;
	while (cmd)
	{
		if (cmd->fd_in == 0)
			cmd->fd_in = fdlist[i];
		else
		{
			if (fdlist[i] != 0)
				close(fdlist[i]);
		}
		if (cmd->fd_out == 1)
			cmd->fd_out = fdlist[i + 1];
		else
		{
			if (fdlist[i + 1] != 1)
				close(fdlist[i + 1]);
		}
		i += 2;
		cmd = cmd->next;
	}
}

void	pipe_cmd(t_cmd *cmd, int count)
{
	int		*fdlist;
	size_t	i;

	fdlist = new_pipe(count - 1);
	fd_apply(cmd, fdlist);
	i = 0;
	while (cmd)
	{
		exec(cmd->argv, cmd->fd_in, cmd->fd_out, fdlist);
		cmd = cmd->next;
		i++;
	}
	free(fdlist);
}

void	exec_cmd(t_cmd *cmd)
{
	int		count;
	int		status;
	size_t	i;

	if (!cmd->argv[0][0])
		return ;
	g_pidlist = new_pidlist();
	if ((count = cmd_count(cmd)) == 1)
		exec(cmd->argv, cmd->fd_in, cmd->fd_out, NULL);
	else
		pipe_cmd(cmd, count);
	i = 0;
	while (i < g_pidlist->count)
	{
		if (g_pidlist->list[0] != 0)
		{
			waitpid(g_pidlist->list[i], &status, 0);
			g_status = WEXITSTATUS(status);
		}
		i++;
	}
	delete_pidlist();
}

void	exec_line(t_token *token)
{
	int		index;

	index = 0;
	g_cmd = NULL;
	parse_cmd(token, &index);
	while (g_cmd)
	{
		exec_cmd(g_cmd);
		delete_cmd(g_cmd);
		g_cmd = NULL;
		parse_cmd(token, &index);
	}
}
