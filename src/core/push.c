/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:30:58 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:15:25 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "sys/types.h"
#include <fcntl.h>
#include "ft_stdio.h"
#include "unistd.h"
#include "builtins.h"
#include "pid.h"
#include "utils.h"
#include "minishell.h"
#include "sys/wait.h"
#include "ft_basic_string.h"
#include "ft_etc.h"
#include "parser.h"

t_token	*pushstr(t_string *str, t_token *token, int *is_append)
{
	string_append(str, token->str);
	*is_append = 1;
	return (token->next);
}

t_token	*pushenv(t_string *str, t_token *token, int *is_append)
{
	char	*tmp;

	tmp = strenv(token->str);
	if (tmp[0])
	{
		string_append(str, tmp);
		*is_append = 1;
	}
	else
	{
		string_append(str, "");
		*is_append = 1;
	}
	free(tmp);
	return (token->next);
}

t_token	*pushspace(t_string *str, t_vector *argv, t_token *token,
					int *is_append)
{
	if (*is_append)
		vector_push_str(argv, str->ptr);
	string_clear(str);
	*is_append = 0;
	return (token->next);
}

t_token	*pushpipe(t_string *str, t_token *token,
				t_vector **argv, int *is_append)
{
	if (*is_append)
		vector_push_str(*argv, str->ptr);
	string_clear(str);
	cmd_push(g_cmd, (char**)vector_cp_ptr(*argv));
	delete_vector(*argv);
	*argv = new_vector();
	*is_append = 0;
	return (token->next);
}

t_token	*pushredir(t_token *token)
{
	char		*tmp;
	t_toktype	type;

	type = token->type;
	token = token->next;
	if (token->type == T_SPACE)
		token = token->next;
	tmp = get_next_token(&token);
	if (type == T_REDIR_IN)
		g_fd[0] = open(tmp, O_RDONLY);
	else if (type == T_REDIR_OUT)
		g_fd[1] = open(tmp, O_RDWR | O_CREAT | O_TRUNC, 0664);
	else
		g_fd[1] = open(tmp, O_RDWR | O_APPEND | O_CREAT, 0664);
	free(tmp);
	return (token);
}
