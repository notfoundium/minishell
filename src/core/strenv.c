/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:33:03 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:17:02 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "ft_stdio.h"
#include "builtins.h"
#include "pid.h"
#include "utils.h"
#include "minishell.h"
#include "sys/wait.h"
#include "ft_basic_string.h"
#include "ft_etc.h"
#include "parser.h"

char	*strenv(char *key)
{
	char	*value;

	if (!ft_strcmp(key, "?"))
	{
		value = ft_calloc(16, sizeof(char));
		ft_itoa(g_status, value, 16);
		return (value);
	}
	if (get_env(key))
		value = ft_strdup(get_env(key));
	else
		value = ft_strdup("");
	return (value);
}
