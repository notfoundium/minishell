/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:22:32 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:16:26 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include "minishell.h"
#include "pid.h"

static void	signal_handler(int sig)
{
	signal(sig, signal_handler);
}

void		signal_dispatcher(void)
{
	signal(SIGINT, signal_handler);
	signal(SIGQUIT, signal_handler);
}
