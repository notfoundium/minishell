/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:19:02 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 03:19:04 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "token.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "ft_stdio.h"
#include "ft_etc.h"

t_token	*new_token(void)
{
	t_token	*tok;

	tok = (t_token*)ft_malloc(sizeof(t_token));
	tok->str = NULL;
	tok->next = NULL;
	return (tok);
}

void	token_push(t_token *tok, char *str, int type)
{
	if (!tok->str)
	{
		tok->str = ft_strdup(str);
		tok->type = type;
	}
	else
	{
		while (tok->next)
			tok = tok->next;
		tok->next = (t_token*)ft_malloc(sizeof(t_token));
		tok = tok->next;
		tok->str = ft_strdup(str);
		tok->type = type;
		tok->next = NULL;
	}
}

void	token_print(t_token *token)
{
	char	*str;

	ft_write(1, "{ ", 2);
	while (token)
	{
		str = ft_calloc(16, sizeof(char));
		ft_write(1, "[", 1);
		ft_write(1, token->str, ft_strlen(token->str));
		ft_write(1, "]", 2);
		ft_write(1, "(", 1);
		ft_write(1, ft_itoa(token->type, str, 16), 16);
		ft_write(1, ") ", 2);
		token = token->next;
		free(str);
	}
	ft_write(1, "}\n", 2);
}

void	delete_token(t_token *token)
{
	t_token	*tmp;

	while (token)
	{
		free(token->str);
		tmp = token;
		token = token->next;
		free(tmp);
	}
}

void	token_pop(t_token *token)
{
	t_token	*prev;

	if (!token->str)
		return ;
	if (!token->next)
	{
		free(token->str);
		return ;
	}
	while (token->next)
	{
		prev = token;
		token = token->next;
	}
	free(token->str);
	free(token);
	prev->next = NULL;
}
