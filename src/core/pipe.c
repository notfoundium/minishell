/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipe.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 03:22:01 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:14:55 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_etc.h"

int	*new_pipe(int pipe_count)
{
	int		*arr_fd;
	int		i;
	int		pipe_fd[2];

	if (pipe_count == 0)
		return (NULL);
	arr_fd = (int*)ft_calloc(pipe_count * 2 + 2, sizeof(int));
	i = 0;
	arr_fd[0] = 0;
	while (i < pipe_count)
	{
		if (pipe(pipe_fd) < 0)
			quit("pipe");
		arr_fd[i * 2 + 1] = pipe_fd[1];
		arr_fd[i * 2 + 2] = pipe_fd[0];
		i++;
	}
	arr_fd[i * 2 + 1] = 1;
	return (arr_fd);
}
