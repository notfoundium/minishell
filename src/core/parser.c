/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 20:23:16 by mwillena          #+#    #+#             */
/*   Updated: 2021/01/10 20:23:36 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "sys/types.h"
#include "ft_stdio.h"
#include "unistd.h"
#include "builtins.h"
#include "pid.h"
#include "utils.h"
#include "minishell.h"
#include "sys/wait.h"
#include "ft_basic_string.h"
#include "ft_etc.h"

int		new_parser(t_string **str, t_vector **argv)
{
	g_cmd = new_cmd();
	*argv = new_vector();
	*str = new_string(0);
	return (0);
}

int		delete_parser(t_string **str, t_vector **argv, int flag)
{
	delete_string(*str);
	delete_vector(*argv);
	if (flag)
		delete_cmd(g_cmd);
	return (0);
}
