/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_token.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:36:42 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:06:57 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "ft_stdlib.h"
#include "ft_string.h"
#include "sys/types.h"
#include "ft_stdio.h"
#include "unistd.h"
#include "builtins.h"
#include "pid.h"
#include "utils.h"
#include "minishell.h"
#include "sys/wait.h"
#include "ft_basic_string.h"
#include "ft_etc.h"
#include "parser.h"

char	*get_next_token(t_token **token)
{
	t_string	*str;
	char		*ret;
	char		*value;

	str = new_string(0);
	while (*token)
	{
		if ((*token)->type == T_STR)
			string_append(str, (*token)->str);
		else if ((*token)->type == T_ENV)
		{
			value = strenv((*token)->str);
			string_append(str, value);
			free(value);
		}
		else
			break ;
		*token = (*token)->next;
	}
	ret = string_c_str(str);
	delete_string(str);
	return (ret);
}
