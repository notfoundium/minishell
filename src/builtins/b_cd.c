/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_cd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 01:22:12 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:02:09 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "builtins.h"
#include "minishell.h"
#include "ft_stdio.h"
#include "ft_string.h"
#include "ft_vector.h"
#include "env.h"

static int	cd_empty(void)
{
	char	*home;
	int		res;

	home = get_env("HOME");
	if (!home)
	{
		ft_write(2, "cd: HOME not set\n", 17);
		return (1);
	}
	res = chdir(home);
	if (res < 0)
		ft_perror("cd: ");
	return (res);
}

static void	cd_set_pwd(char *old)
{
	char	*tmp;
	char	*path;

	env_add(tmp = ft_strmerge("OLDPWD=", old));
	free(tmp);
	path = get_wd();
	env_add(tmp = ft_strmerge("PWD=", path));
	free(path);
	free(tmp);
	free(old);
}

static int	argcount(char **argv)
{
	int		ac;

	ac = 0;
	while (argv[ac])
		ac++;
	if (ac > 2)
	{
		ft_write(2, "cd: too many arguments\n", 23);
		return (-1);
	}
	return (ac);
}

static int	return_error(char *oldpwd)
{
	ft_write(2, "cd: OLDPWD not set\n", 19);
	free(oldpwd);
	return (1);
}

int			b_cd(char **argv)
{
	int		ac;
	int		res;
	char	*path;
	char	*old;

	if (!(path = argv[1]))
		return (cd_empty());
	if ((ac = argcount(argv)) < 0)
		return (1);
	old = get_wd();
	if (!ft_strcmp(path, "-"))
	{
		if (!(path = get_env("OLDPWD")))
			return (return_error(old));
		ft_puts(path);
	}
	if ((res = chdir(path)) < 0)
	{
		free(old);
		ft_write(2, "cd: ", 4);
		ft_perror(path);
		return (1);
	}
	cd_set_pwd(old);
	return (res);
}
