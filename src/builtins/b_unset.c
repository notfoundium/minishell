/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_unset.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 01:22:53 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:04:41 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"
#include "env.h"
#include "ft_string.h"
#include "ft_stdio.h"
#include "ft_ctype.h"

void		print_error(char *pair)
{
	ft_write(2, "unset: ", 7);
	ft_write(2, pair, ft_strlen(pair));
	ft_write(2, ": not a valid identifier\n", 25);
}

static int	is_valid(char *pair)
{
	int	i;

	i = 0;
	if (!ft_isalpha(pair[i]) && pair[i] != '_')
		return (0);
	i++;
	while (pair[i])
	{
		if (!ft_isalnum(pair[i]) && !(pair[i] == '_'))
			return (0);
		i++;
	}
	return (1);
}

int			b_unset(char **av)
{
	size_t	i;

	i = 1;
	while (av[i])
	{
		if (!is_valid(av[i]))
			print_error(av[i]);
		else
			ft_unsetenv(av[i]);
		i++;
	}
	return (0);
}
