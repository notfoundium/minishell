/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_export.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 01:22:40 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:03:27 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "builtins.h"
#include "env.h"
#include "ft_string.h"
#include "ft_ctype.h"
#include "ft_stdio.h"

static int	insert_value(char **pos, const char *pair)
{
	size_t	old_len;
	char	*tmp;
	int		i;

	i = 0;
	while (pair[i] && pair[i] != '=')
		i++;
	if (pair[i] == '=')
	{
		old_len = ft_strlen(*pos);
		tmp = *pos;
		*pos = ft_strdup(pair);
		free(tmp);
	}
	return (0);
}

static int	is_valid(const char *pair)
{
	size_t	i;
	size_t	key_len;

	i = 0;
	key_len = get_key_len(pair);
	if (key_len == 0)
		return (0);
	if (ft_isdigit(pair[0]))
		return (0);
	while (i < key_len)
	{
		if (pair[i] != '_' && !ft_isalnum(pair[i]))
			return (0);
		i++;
	}
	return (1);
}

int			env_add(char *pair)
{
	size_t	size;
	size_t	current_len;
	size_t	key_len;

	if (!is_valid(pair))
		return (1);
	size = 0;
	key_len = get_key_len(pair);
	while (g_env->ptr[size])
	{
		current_len = get_key_len(g_env->ptr[size]);
		if (current_len == key_len)
		{
			if (!ft_strcmp(g_env->ptr[size], pair))
				return (0);
			if (!ft_strncmp(g_env->ptr[size], pair, key_len))
				return (insert_value((char**)g_env->ptr + size, pair));
		}
		size++;
	}
	vector_push_str(g_env, pair);
	return (0);
}

static void	export_print_env(int fd_out)
{
	size_t	i;

	i = 0;
	while (g_env->ptr[i])
	{
		ft_write(fd_out, "declare -x ", 11);
		ft_write(fd_out, g_env->ptr[i], ft_strlen(g_env->ptr[i]));
		ft_write(fd_out, "\n", 1);
		i++;
	}
	if (fd_out != 1)
		close(fd_out);
}

int			b_export(char **av, int fd_out)
{
	int	i;
	int	len;
	int	ret;

	i = 1;
	ret = 0;
	if (av[1] == NULL)
		export_print_env(fd_out);
	else
		while (av[i])
		{
			if (env_add(av[i]))
			{
				ft_write(2, "export: ", 8);
				len = get_key_len(av[i]);
				ft_write(2, av[i], ft_strlen(av[i]));
				ft_write(2, ": not a valid identifier\n", 25);
				ret = 1;
				i++;
			}
			else
				i++;
		}
	return (ret);
}
