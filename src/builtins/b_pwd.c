/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_pwd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 01:22:46 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:04:06 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "minishell.h"
#include "builtins.h"
#include "ft_stdio.h"
#include "ft_string.h"

char	*get_wd(void)
{
	char	*buf;
	char	*res;
	size_t	size;

	size = 256;
	buf = (char*)ft_calloc(size, sizeof(char));
	res = getcwd(buf, size);
	while (!res)
	{
		free(buf);
		size *= 2;
		buf = ft_calloc(size, sizeof(char));
		res = getcwd(buf, size);
	}
	return (res);
}

int		b_pwd(void)
{
	char	*res;

	res = get_wd();
	ft_write(1, res, ft_strlen(res));
	ft_write(1, "\n", 1);
	free(res);
	return (0);
}
