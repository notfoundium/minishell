/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_env.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 01:22:28 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/10 01:22:29 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"
#include "env.h"
#include "ft_string.h"
#include "ft_stdio.h"

static int	is_init(char *pair)
{
	size_t	i;

	i = 0;
	while (pair[i])
	{
		if (pair[i++] == '=')
			return (1);
	}
	return (0);
}

int			b_env(void)
{
	int	i;

	i = 0;
	while (g_env->ptr[i])
	{
		if (is_init(g_env->ptr[i]))
		{
			ft_write(1, g_env->ptr[i], ft_strlen(g_env->ptr[i]));
			ft_write(1, "\n", 1);
		}
		i++;
	}
	return (0);
}
