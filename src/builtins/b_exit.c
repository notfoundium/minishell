/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_exit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 01:22:36 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/16 20:53:53 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"
#include "minishell.h"
#include "ft_ctype.h"
#include "ft_stdio.h"
#include "ft_string.h"

static int	is_valid_arg(const char *arg)
{
	int	i;

	i = 0;
	if (!ft_isdigit(arg[i]) && arg[i] != '-' && arg[i] != '+')
		return (0);
	while (arg[++i])
	{
		if (!ft_isdigit(arg[i]))
			return (0);
	}
	return (1);
}

int			b_exit(char **av)
{
	unsigned int	code;
	size_t			ac;

	if (!av[1])
		exit(g_status);
	if (!is_valid_arg(av[1]))
	{
		ft_write(2, "exit: ", 6);
		ft_write(2, av[1], ft_strlen(av[1]));
		ft_write(2, ": numeric argument required\n", 28);
		exit(2);
	}
	code = (unsigned int)ft_atoi(av[1]);
	ac = 0;
	while (av[ac])
		ac++;
	if (ac > 2)
	{
		ft_write(2, "exit: too many arguments\n", 25);
		return (1);
	}
	exit(code % 256);
}
