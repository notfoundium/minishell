/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_echo.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 01:22:24 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 20:01:46 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"
#include "ft_stdio.h"
#include "ft_string.h"

static int	skip_options(int *i)
{
	*i = *i + 1;
	return (1);
}

int			b_echo(char **av)
{
	int	i;
	int	n;

	i = 1;
	n = 0;
	if (!av[1])
	{
		ft_write(1, "\n", 1);
		return (0);
	}
	while (av[i])
		if (!ft_strcmp(av[i], "-n"))
			n = skip_options(&i);
		else
			break ;
	while (av[i])
	{
		ft_write(1, av[i], ft_strlen(av[i]));
		if (av[++i])
			ft_write(1, " ", 1);
	}
	if (!n)
		ft_write(1, "\n", 1);
	return (0);
}
