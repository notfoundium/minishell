/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stdlib.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:04:51 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:04:53 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STDLIB_H
# define FT_STDLIB_H

# include <stdlib.h>
# include "ft_vector.h"

t_vector	*g_env;

void		*ft_malloc(size_t size);
void		*ft_calloc(size_t nmemb, size_t size);
char		*ft_getenv(const char *name);
int			ft_unsetenv(const char *name);
int			ft_atoi(const char *nptr);

/*
** Broken
*/
int			ft_setenv(const char *name, const char *value,
			int overwrite);

#endif
