/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_etc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:04:34 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:04:36 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ETC_H
# define FT_ETC_H

# include "ft_stdlib.h"

char	*ft_itoa(int num, char *buf, size_t size);
void	quit(const char *info);

#endif
