/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stdio.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:05:02 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:05:05 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STDIO_H
# define FT_STDIO_H

# ifndef EOF
#  define EOF -1
# endif

# include "ft_stdlib.h"

int		ft_write(int fd, const char *buf, size_t count);
int		ft_getchar(void);
int		ft_puts(const char *s);
void	ft_perror(const char *s);

#endif
