/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_basic_string.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 02:49:39 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 02:49:41 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BASIC_STRING_H
# define FT_BASIC_STRING_H

# include "ft_string.h"

typedef struct	s_string
{
	char	*ptr;
	size_t	size;
}				t_string;

t_string		*new_string(const char *str);
void			delete_string(t_string *string);
char			*string_c_str(t_string *str);
void			string_clear(t_string *str);
void			string_resize(t_string *string, size_t size);
void			string_pushback(t_string *string, char c);
void			string_append(t_string *string, char *appendix);
void			string_print(t_string *string);

#endif
