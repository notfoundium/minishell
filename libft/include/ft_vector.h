/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:06:35 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:06:36 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_VECTOR_H
# define FT_VECTOR_H

# include <stdlib.h>

typedef struct	s_vector
{
	void		**ptr;
	size_t		size;
}				t_vector;

t_vector		*new_vector(void);
void			delete_vector(t_vector *vector);
void			vector_resize(t_vector *vector);
size_t			vector_get_len(t_vector *vector);
void			**vector_cp_ptr(t_vector *vector);
void			vector_push(t_vector *vector, void *element);
void			vector_push_str(t_vector *vector,
								const char *str);
void			vector_remove(t_vector *vector, size_t index);
int				vector_find_str(t_vector *vector,
								const char *str);
void			vector_print(t_vector *vector);

#endif
