/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:07:47 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:07:49 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etc.h"
#include "ft_string.h"
#include "ft_stdio.h"

void	quit(const char *info)
{
	if (info)
		ft_write(2, info, ft_strlen(info));
	ft_perror("Error status:\n");
	exit(0);
}
