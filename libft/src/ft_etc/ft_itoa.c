/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 08:36:54 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/03 08:36:58 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_etc.h"
#include "ft_string.h"

static size_t	get_num_size(int num)
{
	size_t	size;

	size = (num > 0) ? (1) : (2);
	while (num > 9)
	{
		num /= 10;
		size++;
	}
	return (size);
}

static void		arr_init(char *arr)
{
	int	i;

	i = 0;
	while (i < 12)
		arr[i++] = 0;
}

char			*ft_itoa(int num, char *buf, size_t size)
{
	char	tmp[12];
	size_t	i;
	int		t_i;

	if (size <= get_num_size(num))
		return (NULL);
	if (num == -2147483648)
	{
		ft_strlcpy(buf, "-2147483648", 12);
		return (buf);
	}
	arr_init(tmp);
	i = 0;
	(num == 0) ? (buf[0] = '0') : (0);
	(num < 0) ? (buf[i++] = '-') : (0);
	(num < 0) ? (num = -num) : (0);
	t_i = 0;
	while (num > 0)
	{
		tmp[t_i++] = '0' + num % 10;
		num /= 10;
	}
	while (--t_i >= 0)
		buf[i++] = tmp[t_i];
	return (buf);
}
