/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_vector.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:08:25 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:08:26 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"
#include "ft_stdlib.h"

t_vector	*new_vector(void)
{
	t_vector	*vector;

	vector = (t_vector*)ft_malloc(sizeof(t_vector));
	vector->ptr = (void**)ft_calloc(sizeof(void*), 1);
	vector->size = 1;
	return (vector);
}
