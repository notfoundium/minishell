/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_find_str.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:08:57 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:08:57 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"
#include "ft_string.h"

int	vector_find_str(t_vector *vector, const char *str)
{
	int	i;

	i = 0;
	while (vector->ptr[i])
	{
		if (!ft_strcmp((char*)vector->ptr[i], str))
			return (i);
		i++;
	}
	return (-1);
}
