/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_get_len.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:37:44 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:37:45 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"

size_t	vector_get_len(t_vector *vector)
{
	size_t	len;

	len = 0;
	while (vector->ptr[len])
		len++;
	return (len);
}
