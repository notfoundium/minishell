/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_remove.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:08:52 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:08:52 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"

void		vector_remove(t_vector *vector, size_t index)
{
	void	*tmp;

	if (index >= vector->size)
		return ;
	if (!vector->ptr[index])
		return ;
	tmp = vector->ptr[index];
	index++;
	while (vector->ptr[index])
	{
		vector->ptr[index - 1] = vector->ptr[index];
		index++;
	}
	vector->ptr[index - 1] = NULL;
	free(tmp);
}
