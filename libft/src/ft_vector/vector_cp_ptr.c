/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_cp_ptr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:38:38 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:38:38 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"
#include "ft_string.h"
#include "ft_stdlib.h"

void	**vector_cp_ptr(t_vector *vector)
{
	void	**copy;
	size_t	i;

	copy = (void**)ft_calloc(vector->size, sizeof(char*));
	i = 0;
	while (vector->ptr[i])
	{
		copy[i] = ft_strdup((char*)vector->ptr[i]);
		i++;
	}
	return (copy);
}
