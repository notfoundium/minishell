/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_resize.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:09:10 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:09:11 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"
#include "ft_stdlib.h"

void		vector_resize(t_vector *vector)
{
	void	**new_ptr;
	size_t	i;

	vector->size *= 2;
	new_ptr = (void**)ft_calloc(vector->size, sizeof(void*));
	i = 0;
	while (vector->ptr[i])
	{
		new_ptr[i] = vector->ptr[i];
		i++;
	}
	free(vector->ptr);
	vector->ptr = new_ptr;
}
