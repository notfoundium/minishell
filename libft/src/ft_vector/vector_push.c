/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_push.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:08:41 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:08:42 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"

void		vector_push(t_vector *vector, void *element)
{
	size_t	i;

	i = 0;
	while (vector->ptr[i])
		i++;
	if (vector->size <= i + 2)
		vector_resize(vector);
	vector->ptr[i] = element;
}
