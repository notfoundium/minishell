/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_print.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:08:46 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:08:47 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"
#include "ft_stdio.h"

void		vector_print(t_vector *vector)
{
	int	i;

	i = 0;
	while (vector->ptr[i])
		ft_puts((char*)vector->ptr[i++]);
}
