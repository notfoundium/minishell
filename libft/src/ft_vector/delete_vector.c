/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_vector.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:08:31 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:08:31 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_vector.h"

void		delete_vector(t_vector *vector)
{
	size_t	i;

	i = 0;
	while (vector->ptr[i])
		free(vector->ptr[i++]);
	free(vector->ptr);
	free(vector);
}
