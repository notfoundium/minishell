/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mwillena <mwillena@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:09:26 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/11 01:53:37 by mwillena         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_stdio.h"

int					ft_getchar(void)
{
	unsigned char	c;
	int				res;

	res = read(0, &c, 1);
	if (res > 0)
		return ((int)c);
	return (EOF);
}
