/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puts.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:09:37 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:09:38 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_stdio.h"
#include "ft_string.h"

int	ft_puts(const char *s)
{
	int	len;

	len = ft_write(1, s, ft_strlen(s));
	return (len + ft_write(1, "\n", 1));
}
