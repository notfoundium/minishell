/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_perror.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:09:32 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:09:33 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <string.h>
#include "ft_stdio.h"
#include "ft_string.h"

void	ft_perror(const char *s)
{
	char	*error;

	error = strerror(errno);
	ft_write(2, s, ft_strlen(s));
	ft_write(2, ": ", 2);
	ft_write(2, error, ft_strlen(error));
	ft_write(2, "\n", 1);
}
