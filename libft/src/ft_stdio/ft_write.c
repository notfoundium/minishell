/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_write.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:09:43 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:09:44 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_stdio.h"
#include "ft_etc.h"

int	ft_write(int fd, const char *buf, size_t count)
{
	ssize_t	printed;
	ssize_t	res;

	printed = 0;
	while (count > 0)
	{
		res = write(fd, buf, count);
		if (res < 0)
			quit("Failed to write\n");
		count -= res;
		buf += res;
		printed += res;
	}
	return (printed);
}
