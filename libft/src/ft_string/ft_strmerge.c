/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmerge.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:38:00 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:38:01 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strmerge(const char *str1, const char *str2)
{
	char	*merged;
	size_t	len_1;
	size_t	len_2;

	if (!str1 && !str2)
		return (NULL);
	len_1 = (str1) ? (ft_strlen(str1)) : (0);
	len_2 = (str2) ? (ft_strlen(str2)) : (0);
	merged = (char*)ft_calloc(len_1 + len_2 + 1, sizeof(char));
	ft_strlcpy(merged, str1, len_1 + 1);
	ft_strlcpy(merged + len_1, str2, len_2 + 1);
	return (merged);
}
