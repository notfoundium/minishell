/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:11:21 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:11:22 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	int				diff;
	unsigned char	c1;
	unsigned char	c2;

	if (n == 0)
		return (0);
	while (*s1 == *s2 && *s1 && *s2 && --n > 0)
	{
		s1++;
		s2++;
	}
	c1 = *s1;
	c2 = *s2;
	diff = c1 - c2;
	return (diff);
}
