/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:37:28 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:37:29 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	size_t	i;
	size_t	j;

	if (!haystack || !needle)
		return (NULL);
	i = 0;
	j = 0;
	while (needle[i])
	{
		if (haystack[j] == 0)
			return (NULL);
		if (haystack[j] == needle[i])
			i++;
		else
			i = 0;
		j++;
	}
	return ((char*)(haystack + j - 1));
}
