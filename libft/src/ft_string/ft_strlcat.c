/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:11:12 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:11:13 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	const char	*p_dst;
	const char	*p_src;
	size_t		i;
	size_t		dst_len;

	p_dst = dst;
	p_src = src;
	i = size;
	while (i-- != 0 && *dst != 0)
		dst++;
	dst_len = dst - p_dst;
	i = size - dst_len;
	if (i-- == 0)
		return (dst_len + ft_strlen(src));
	while (*src)
	{
		if (i != 0)
		{
			*dst++ = *src;
			i--;
		}
		src++;
	}
	*dst = 0;
	return (dst_len + (src - p_src));
}
