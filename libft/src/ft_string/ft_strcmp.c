/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:10:51 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:10:52 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

int		ft_strcmp(const char *std_s1, const char *std_s2)
{
	size_t	i;

	if (!std_s1 || !std_s2)
		return (0);
	i = 0;
	while (std_s1[i] && std_s2[i] && std_s1[i] == std_s2[i])
		i++;
	return (std_s1[i] - std_s2[i]);
}
