/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_resize.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 02:52:47 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 02:52:47 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_basic_string.h"

void	string_resize(t_string *string, size_t size)
{
	char	*tmp;

	if (size == string->size)
		return ;
	tmp = string->ptr;
	string->ptr = (char*)ft_calloc(size, sizeof(char));
	if (size > string->size)
		ft_strlcpy(string->ptr, tmp, string->size);
	else
		ft_strlcpy(string->ptr, tmp, size);
	free(tmp);
	string->size = size;
}
