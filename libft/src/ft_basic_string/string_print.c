/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_print.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 02:52:20 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 02:52:21 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_basic_string.h"
#include "ft_stdio.h"

void	string_print(t_string *string)
{
	ft_puts(string->ptr);
}
