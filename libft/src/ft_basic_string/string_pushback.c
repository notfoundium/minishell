/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_pushback.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 02:52:37 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 02:52:38 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_basic_string.h"

void	string_pushback(t_string *string, char c)
{
	size_t	len;

	len = ft_strlen(string->ptr);
	if (len + 1 >= string->size)
		string_resize(string, string->size * 2);
	string->ptr[len] = c;
}
