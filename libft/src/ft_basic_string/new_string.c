/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_string.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 02:50:37 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 02:50:39 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_basic_string.h"

t_string	*new_string(const char *str)
{
	t_string	*string;

	string = (t_string*)ft_malloc(sizeof(t_string));
	if (!str)
	{
		string->ptr = (char*)ft_calloc(1, sizeof(char));
		string->size = 1;
	}
	else
	{
		string->ptr = ft_strdup(str);
		string->size = ft_strlen(str) + 1;
	}
	return (string);
}
