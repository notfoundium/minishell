/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_append.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 02:51:05 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 02:51:06 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_basic_string.h"

void	string_append(t_string *string, char *appendix)
{
	size_t	i;

	if (!appendix)
		return ;
	if (!appendix[0])
		return ;
	i = 0;
	while (appendix[i])
		string_pushback(string, appendix[i++]);
}
