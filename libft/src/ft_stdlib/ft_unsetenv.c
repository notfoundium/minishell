/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:39:01 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:39:02 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdlib.h"
#include "ft_string.h"

static int	is_find(const char *key, const char *pair)
{
	size_t	keylen;

	keylen = ft_strlen(key);
	if (!ft_strncmp(key, pair, keylen))
		return (1);
	return (0);
}

int			ft_unsetenv(const char *name)
{
	size_t	i;

	i = 0;
	while (g_env->ptr[i])
	{
		if (is_find(name, g_env->ptr[i]))
			break ;
		i++;
	}
	if (!g_env->ptr[i])
		return (0);
	vector_remove(g_env, i);
	return (0);
}
