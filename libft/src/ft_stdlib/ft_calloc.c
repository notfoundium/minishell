/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 04:08:10 by gmegga            #+#    #+#             */
/*   Updated: 2020/12/09 04:08:11 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdlib.h"
#include "ft_string.h"

void	*ft_calloc(size_t nmemb, size_t size)
{
	char	*p;

	p = (char*)ft_malloc(nmemb * size);
	ft_bzero(p, nmemb * size);
	return ((void*)p);
}
