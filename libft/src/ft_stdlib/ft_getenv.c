/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:38:20 by gmegga            #+#    #+#             */
/*   Updated: 2021/01/10 19:38:21 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdlib.h"
#include "ft_string.h"

char	*ft_getenv(const char *name)
{
	size_t	i;

	i = 0;
	while (g_env->ptr[i])
	{
		if (ft_strstr(g_env->ptr[i], name) == g_env->ptr[i])
			return (g_env->ptr[i] + ft_strlen(name) + 1);
		i++;
	}
	return (NULL);
}
